var renderer;
var scene;
var camera;
var control;
var mesh;

function init() {
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.x = 15;
    camera.position.y = 16;
    camera.position.z = 13;
    camera.lookAt(scene.position);
    renderer = new THREE.WebGLRenderer();
    renderer.setClearColor(0x000000, 1.0);
    renderer.setSize(window.innerWidth, window.innerHeight);
    var ambientLight = new THREE.AmbientLight(0x090909);
    scene.add(ambientLight);
    var spotLight = new THREE.SpotLight();
    spotLight.position.set(10, 80, 30);
    spotLight.castShadow = true;
    scene.add(spotLight);
    document.body.appendChild(renderer.domElement);
    control = new function () {
        this.rotSpeed = 0.005;
    };
    render();
};

function render() {
    renderer.render(scene, camera);
    var x = camera.position.x;
    var z = camera.position.z;
    camera.position.x = x * Math.cos(control.rotSpeed) + z * Math.sin(control.rotSpeed);
    camera.position.z = z * Math.cos(control.rotSpeed) - x * Math.sin(control.rotSpeed);
    camera.lookAt(scene.position);
    requestAnimationFrame(render);
    getSpans();
};

function getSpans() {
    squareSpan = document.getElementById('square');
    volumeSpan = document.getElementById('volume');
}

function clearScene() {
    scene.remove(mesh);
}

function MeshObject(type, length, depth, height, radius) {
    this.type = type;
    this.length = length;
    this.depth = depth;
    this.height = height;
    this.radius = radius;

    this.createMesh = function() {
        switch(type) {
            case 'box':
                var geometry = new THREE.BoxGeometry(this.length, this.depth, this.height);
                var material = new THREE.MeshLambertMaterial({color: 0xff0000});
                break;
            case 'sphere':
                var geometry = new THREE.SphereGeometry(this.radius, 30, 30);
                var material = new THREE.MeshPhongMaterial({color: 0x00ff00});
                break;
            case 'cylinder':
                var geometry = new THREE.CylinderGeometry(this.radius, this.radius, this.height, 50);
                var material = new THREE.MeshLambertMaterial({color: 0x0000ff});
                break;
            default:break;
        };
        mesh = new THREE.Mesh(geometry, material);
    };

    this.addOnScene = function() {
        scene.add(mesh);
    };

    this.calculateSquare = function() {
        switch(type) {
            case 'box':
                var square = 2 * (this.length * this.depth + this.length * this.height + this.depth * this.height);
                squareSpan.textContent = square.toFixed(2);
                break;
            case 'sphere':
                var square = Math.PI * 4 * (Math.pow(this.radius, 2));
                squareSpan.textContent = square.toFixed(2);
                break;
            case 'cylinder':
                var square = 2 * Math.PI * radius * (this.radius + this.height);
                squareSpan.textContent = square.toFixed(2);
                break;
            default: break;
        };
    };
    
    this.calculateVolume = function() {
        switch(type) {
            case 'box':
                var volume = this.length * this.depth * this.height;
                volumeSpan.textContent = volume.toFixed(2);
                break;
            case 'sphere':
                var volume = 4 / 3 * Math.PI * (Math.pow(this.radius, 3));
                volumeSpan.textContent = volume.toFixed(2);
                break;
            case 'cylinder':
                var volume = Math.PI * this.height * (Math.pow(this.radius, 2));
                volumeSpan.textContent = volume.toFixed(2);
                break;
            default:break;
        };
    };
    
};

function addMesh(object) {
    var length = (document.getElementById(`${object}Length`) || {}).value || 4;
    var depth = (document.getElementById(`${object}Depth`) || {}).value || 4;
    var height = (document.getElementById(`${object}Height`) || {}).value || 4;
    var radius = (document.getElementById(`${object}Radius`) || {}).value || 2;
    var obj = new MeshObject(object, length, depth, height, radius);
    clearScene();  
    obj.createMesh();
    obj.addOnScene();
    obj.calculateSquare();
    obj.calculateVolume();
};

window.onload = init;